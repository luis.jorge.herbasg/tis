----------
comando para crear imagen Docker del servidor
-------------------
docker build -t php_server .
-------------------
Nota: el punto "." es parte del comando 
<build> => crea una imagen docker en base al contenido de el 
Dockerfile(una imagen docker es como una imagen iso de un sistema operativo)
<-t [name]> => sirve para asignar nombre a la imagen Docker que crearemos
<.> => indica que Dockerfile se encuentra en el directorio actual  
-----------------
comando para hacer correr el servidor
-----------------
docker run -p 9090:80 -v C:\Users\pc\Desktop\TIS:/var/www/html --name=tis php_server
----------------
<-p> => indica los puertos "9090" es el puerto que usaremos en nuestra maquina y "80" es el puerto del contenedor 
<-v> => sirve para crear un volumen permitiendo la persistencia de datos en un contenedor docker emparejando 
        carpetas.
<--name [name]> => sirve para asignar un nombre a nuestro contenedor
php_server es el numbre de nuestra imagen indicando que coreremos esa imagen
---------------
comando para ejecutar script en contenedor 
--------------
docker exec tis /bin/bash
---------------
docker exec <nombre_del_cntenedor> <script>
tis  en este caso  el nombre del contenedor al cual ejecutaremos algun script
 