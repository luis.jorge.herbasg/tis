<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemDep extends Model
{
    public $timestamps = false;
    protected $connection = 'mysql';

    protected $fillable = [

        'nombre', 'nivel', 'electiva','departamento_id'
    ];

}
