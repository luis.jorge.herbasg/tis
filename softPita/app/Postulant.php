<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Postulant extends Model
{
    protected $connection = 'mysql';

    public $timestamps = false;
    protected $fillable = ['cod_sis', 'name', 'last_name',
                           'address', 'phone', 'nro_documents','ci',
                           'nro_certificates', 'subject_application',
                           'requerimiento_id', 'id_status', 'observaciones'];

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
    public static function postulantes($id){

        $postulantes = DB::table('convocatorias')->where('convocatorias.subunidad_id', $id)
            ->join('requerimientos', 'requerimientos.convocatoria_id', '=', 'convocatorias.id')
            ->join('postulants', 'requerimientos.id', '=', 'postulants.requerimiento_id')
            ->join('nota_meritos', 'postulants.id', '=', 'nota_meritos.postulante_id')
            ->select('requerimientos.destino', 'postulants.id', 'postulants.name', 'postulants.last_name', 'nota_meritos.nota')
            ->where('id_status', '2')
            ->orderBy('requerimientos.destino', 'ASC')
            ->get();

        return $postulantes;
    }
    public function notaConocimiento(){

        return $this->hasMany(NotaConocimiento::class);
    }
   
}
