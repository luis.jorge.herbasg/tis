<?php

namespace App\Http\Controllers;

use App\Meritos1;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Meritos1Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($convocatoria_id)
    {
        $meritos1 = DB::table('meritos1s')->where('convocatoria_id', $convocatoria_id)->get();

        return View("meritos1/index", ['meritos1' => $meritos1, 'convocatoria_id' => $convocatoria_id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($convocatoria_id)
    {
        return View("meritos1/create", ['convocatoria_id' => $convocatoria_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $convocatoria_id)
    {
        $campos = [
            'nombre' => 'required|string|max:255',
            'porcentaje' => 'required|integer|min:1|max:100',
        ];
        $Mensaje = ["required" => 'El :attribute es requerido'];

        $this->validate($request, $campos, $Mensaje);

        $nombre = $request->input("nombre");
        $porcentaje = $request->input("porcentaje");
        $merito_nuevo1 = new Meritos1();
        $merito_nuevo1->convocatoria_id = $convocatoria_id;
        $merito_nuevo1->nombre = $nombre;
        $merito_nuevo1->porcentaje = $porcentaje;
        $merito_nuevo1->save();
        return redirect(route("detalle.index", ['convocatoria_id' => $convocatoria_id]))->with('Mensaje', 'Merito creado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function show(Meritos1 $merito1)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function edit($convocatoria_id, $merito1_id)
    {
        $merito1 = Meritos1::findOrFail($merito1_id);

        return View("meritos1/edit", ['merito1' => $merito1, 'convocatoria_id' => $convocatoria_id, 'merito1_id' => $merito1_id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $convocatoria_id, $merito1_id)
    {
        $campos = [
            'nombre' => 'required|string|max:255',
            'porcentaje' => 'required|integer|min:1|max:100',
        ];
        $Mensaje = ["required" => 'El :attribute es requerido'];

        $this->validate($request, $campos, $Mensaje);

        $nombre = $request->input("nombre");
        $porcentaje = $request->input("porcentaje");
        $merito_nuevo1 = Meritos1::findOrFail($merito1_id);
        $merito_nuevo1->convocatoria_id = $convocatoria_id;
        $merito_nuevo1->nombre = $nombre;
        $merito_nuevo1->porcentaje = $porcentaje;
        $merito_nuevo1->save();
        return redirect(route("detalle.index", ['convocatoria_id' => $convocatoria_id]))->with('Mensaje', 'Merito modificado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function destroy($convocatoria_id, $merito1_id)
    {
        Meritos1::destroy($merito1_id);
        return redirect(route("detalle.index", ['convocatoria_id' => $convocatoria_id]))->with('Mensaje', 'Merito eliminado con exito');
    }
}
