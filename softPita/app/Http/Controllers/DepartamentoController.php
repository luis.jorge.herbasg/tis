<?php

namespace App\Http\Controllers;

use App\Departamento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DepartamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $depa = new Departamento;
        $cabeceras = $depa->getTableColumns();
        $departamentos = Departamento::all();
        return view('departamentos.index', ['departamentos' => $departamentos, 'cabeceras' => $cabeceras]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('departamentos.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[
            'name'=>'required|string|max:255|unique:departamentos,name'
    
        ];
        $Mensaje=["required"=>'El :attribute es requerido', "unique"=>'El departamento ya existe'];
        
        $this->validate($request,$campos,$Mensaje);

        $name = $request->input("name");

        $new_departamento = new Departamento();
        $new_departamento->name = $name;

        //dd($new_departamento);

        $new_departamento->save();
        return redirect('departamentos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Departamento  $departamento
     * @return \Illuminate\Http\Response
     */
    public function show(Departamento $departamento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Departamento  $departamento
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departamento = Departamento::where('id', $id)->first();
        return view('departamentos.edit', ['departamento' => $departamento, 'id' => $departamento->id]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Departamento  $departamento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $campos=[
            'name'=>'required|string|max:255|unique:departamentos,name'
    
        ];
        $Mensaje=["required"=>'El :attribute es requerido', "unique"=>'El departamento ya existe'];
        
        $this->validate($request,$campos,$Mensaje);

        $name = $request->input("name");

        $departamento = Departamento::where('id', $id)->first();
        $departamento->name = $name;
        $departamento->save();
        return redirect('departamentos');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Departamento  $departamento
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $departamento = DB::table('departamentos')->where('departamentos.id', $id)
        ->join('subunidades', 'subunidades.id_departamento', 'departamentos.id')
        ->join('convocatorias', 'convocatorias.subunidad_id', 'subunidades.id')
        ->count();

        //dd($departamento);
        if($departamento == 0){
            Departamento::destroy($id);
            return redirect('departamentos')->with('info', 'Departamento eliminado');
        }else {
            return redirect('departamentos')->with('info', 'No es posible eliminar, debido a que contiene convocatorias vigentes');
            }
        
    }
}
