<?php

namespace App\Http\Controllers;

use App\Nota;
use App\Requerimiento;
use App\Conocimiento;
use App\NotaConocimiento;
use App\Convocatoria;
use Doctrine\DBAL\Schema\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class NotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($convocatoria_id, $requerimiento_id)
    {

        $nombre = DB::table('requerimientos')->where('requerimientos.id', $requerimiento_id)
            ->join('convocatorias', 'requerimientos.convocatoria_id', 'convocatorias.id')
            ->join('notas', 'notas.requerimiento_id', 'requerimientos.id')
            ->select('convocatorias.subunidad_id', 'notas.id', 'notas.requerimiento_id', 'requerimientos.destino', 'notas.nombre_nota')
            ->groupby('requerimientos.destino')
            ->orderby('requerimientos.destino', 'DESC')
            ->first();


        $notas = DB::table('requerimientos')->where('requerimientos.id', $requerimiento_id)
            ->join('convocatorias', 'requerimientos.convocatoria_id', 'convocatorias.id')
            ->join('notas', 'notas.requerimiento_id', 'requerimientos.id')
            ->select('convocatorias.subunidad_id', 'notas.id', 'notas.requerimiento_id', 'notas.nombre_nota', 'notas.porcentaje')
            ->orderby('requerimientos.destino', 'DESC')
            ->get();

        return view('notas.index', compact('nombre', 'notas', 'convocatoria_id', 'requerimiento_id'));
    }

    public function indexConocimiento()
    {
        $titulo = 'NOTAS CONOCIMIENTO';
        $requerimientos = Requerimiento::paginate();
        $notas =  Nota::select('notas.id', 'id_postulante', 'last_name', 'name', 'subject_application', 'nota')
            ->join('postulants', 'postulants.id', '=', 'notas.id_postulante')
            ->where('notas.id_tipo_nota', 1)
            ->get();
        $conocimientos = Conocimiento::all();
        return view('notas.conocimiento.index', compact('notas', 'requerimientos', 'titulo', 'conocimientos'));
    }

    public function finales()
    {

        $finales = DB::table('postulants')
            ->join('requerimientos', 'postulants.requerimiento_id', 'requerimientos.id')
            ->join('nota_meritos', 'nota_meritos.postulante_id', 'postulants.id')
            ->join('convocatorias', 'convocatorias.id', 'requerimientos.convocatoria_id')
            ->select('postulants.name', 'postulants.last_name', 'postulants.cod_sis', 'requerimientos.destino', 'postulants.nota_conocimientos', 'nota_meritos.nota', 'postulants.nota_final', 'convocatorias.publicacion_finales')
            ->where('postulants.id_status', 2)
            ->get();
        $convocatorias = DB::table('convocatorias')
            ->groupby('convocatorias.publicacion_finales')
            ->get();


        return View('finales.index', compact('finales', 'convocatorias'));
    }

    public function indexMeritos()
    {
        $requerimientos = Requerimiento::paginate();
        $notas =  Nota::select('notas.id', 'last_name', 'name', 'subject_application', 'nota')
            ->join('postulants', 'postulants.id', '=', 'notas.id_postulante')
            ->where('notas.id_tipo_nota', 2)
            ->get();
        $titulo = 'NOTAS MERITOS';

        return view('notas.meritos.index', compact('notas', 'requerimientos', 'titulo'));
    }

    public function indexNotaFinal()
    {
        $requerimientos = Requerimiento::paginate();
        $notas =  Nota::select('last_name', 'name', 'subject_application', 'nota')
            ->join('postulants', 'postulants.id', '=', 'notas.id_postulante')
            ->where('notas.id_tipo_nota', 3)
            ->get();
        $titulo = 'NOTAS FINALES';

        return view('notas.conocimiento.index', compact('notas', 'requerimientos', 'titulo'));
    }

    public function ingresar(Nota $notaConocimiento)
    {
        return view('notas.conocimiento.ingresar', compact('notaConocimiento'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($convocatoria_id, $requerimiento_id)
    {
        $nota = DB::table('notas')->where('requerimiento_id', $requerimiento_id)->get();
        return View('notas.create', compact('convocatoria_id', 'requerimiento_id', 'nota'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $convocatoria_id, $requerimiento_id)
    {
        $campos = [
            'nombre_nota' => 'required|string|max:255',
            'porcentaje' => 'required|numeric',
        ];
        $Mensaje = ["required" => 'El :attribute es requerido'];

        $this->validate($request, $campos, $Mensaje);

        $nombre_nota = $request->input("nombre_nota");
        $porcentaje = $request->input("porcentaje");
        $nota_nueva = new Nota();
        $nota_nueva->requerimiento_id = $requerimiento_id;
        $nota_nueva->nombre_nota = $nombre_nota;
        $nota_nueva->porcentaje = $porcentaje;

        $nota_nueva->save();

        $postulantes = DB::table('postulants')->where('postulants.requerimiento_id', $requerimiento_id)
            ->select('postulants.id')
            ->get();
        //
        foreach ($postulantes as $postulante) {
            $new_nota = new NotaConocimiento();
            $new_nota->postulante_id = $postulante->id;
            $new_nota->nota_id = $nota_nueva->id;
            $new_nota->save();
        }

        return redirect()->route('nota.index', compact('convocatoria_id', 'requerimiento_id'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Nota  $nota
     * @return \Illuminate\Http\Response
     */
    public function show(Nota $nota)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Nota  $nota
     * @return \Illuminate\Http\Response
     */
    public function editMerito(Nota $notaMerito)
    {
        $notasMerito = Nota::select('notas.id', 'last_name', 'name', 'nota')
            ->join('postulants', 'postulants.id', '=', 'notas.id_postulante')
            ->where('notas.id', $notaMerito->id)
            ->first();
        $requerimientos = Requerimiento::paginate();
        $meritos = DB::table('meritos')->get();
        $meritos1 = DB::table('meritos1s')->get();
        $mertos = DB::table('mertos')->get();
        return view('notas.meritos.edit', compact('notasMerito', 'requerimientos', 'meritos', 'meritos1', 'mertos'));
    }

    public function edit($convocatoria_id, $requerimiento_id, $nota_id)
    {
        $nota = DB::table('notas')->find($nota_id);
        return View('notas.edit', compact('convocatoria_id', 'requerimiento_id', 'nota'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Nota  $nota
     * @return \Illuminate\Http\Response
     */
    public function updateMerito(Request $request, Nota $notaMerito)
    {
        $notaNuevaMerito1 = $request->get('notaPostulante1');
        $notaNuevaMerito2 = $request->get('notaPostulante2');
        $notaNuevaMerito3 = $request->get('notaPostulante3');

        $porcentajeMerito1 = $request->get('porcentaje');
        $porcentajeMerito2 = $request->get('porcentaje1');
        $porcentajeMerito3 = $request->get('porcentaje2');

        $notaMerito->nota = ($notaNuevaMerito1 * ($porcentajeMerito1 / 100) + $notaNuevaMerito2 * ($porcentajeMerito2 / 100) + $notaNuevaMerito3 * ($porcentajeMerito3 / 100));
        $notaMerito->save();

        return redirect()->route('notasMeritos.show', $notaMerito->id)
            ->with('info', 'Nota actualizada con éxito');
    }

    public function update(Request $request, $convocatoria_id, $requerimiento_id, $nota_id)
    {
        $campos = [
            'nombre_nota' => 'required|string|max:255',
            'porcentaje' => 'required|numeric',

        ];
        $Mensaje = ["required" => 'El :attribute es requerido'];

        $this->validate($request, $campos, $Mensaje);

        $nombre_nota = $request->input("nombre_nota");
        $porcentaje = $request->input("porcentaje");
        $nota_nueva = Nota::findOrFail($nota_id);
        $nota_nueva->requerimiento_id = $requerimiento_id;
        $nota_nueva->nombre_nota = $nombre_nota;
        $nota_nueva->porcentaje = $porcentaje;

        $nota_nueva->save();

        return redirect()->route('nota.index', compact('convocatoria_id', 'requerimiento_id'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Nota  $nota
     * @return \Illuminate\Http\Response
     */
    public function destroy($convocatoria_id, $requerimiento_id,  $nota_id)
    {
        Nota::destroy($nota_id);
        return redirect()->route('nota.index', compact('convocatoria_id', 'requerimiento_id'));
    }

    public function filtraje()
    {
        $departamentos = DB::table('departamentos')->get();

        return View('filtraje', compact('departamentos'));
    }


    public function publics()
    {
        $datos = DB::table('convocatorias')
            ->select('convocatorias.publicacion_finales', 'convocatorias.id')
            ->get();

        foreach ($datos as $dato) {
            if ($dato->publicacion_finales == 1) {
                $dato = Convocatoria::find($dato->id);
                $dato->publicacion_finales = 2;
                $dato->save();
            } elseif ($dato->publicacion_finales == 2) {
                $dato = Convocatoria::find($dato->id);
                $dato->publicacion_finales = 1;
                $dato->save();
            }
        }
        return redirect()->route('finales.index');
    }

    public function publicar()
    {
        $finales = DB::table('postulants')
            ->join('requerimientos', 'postulants.requerimiento_id', 'requerimientos.id')
            ->join('nota_meritos', 'nota_meritos.postulante_id', 'postulants.id')
            ->join('convocatorias', 'convocatorias.id', 'requerimientos.convocatoria_id')
            ->select('postulants.name', 'postulants.last_name', 'postulants.cod_sis', 'requerimientos.destino', 'postulants.nota_conocimientos', 'nota_meritos.nota', 'postulants.nota_final', 'convocatorias.publicacion_finales')
            ->where('convocatorias.publicacion_finales', 2)
            ->where('postulants.id_status', 2)
            ->get();

        $inhabilitados = DB::table('postulants')
        ->join('requerimientos', 'postulants.requerimiento_id', 'requerimientos.id')
        ->join('nota_meritos', 'nota_meritos.postulante_id', 'postulants.id')
        ->join('convocatorias', 'convocatorias.id', 'requerimientos.convocatoria_id')
        ->select('postulants.name', 'postulants.last_name', 'postulants.cod_sis', 'requerimientos.destino', 'postulants.nota_conocimientos', 'nota_meritos.nota', 'postulants.nota_final', 'postulants.observaciones', 'convocatorias.publicacion_finales')
        ->where('postulants.id_status', 3)
        ->get();

        
        return view('publicarFinales', compact('finales', 'inhabilitados'));
    }

    public function publicarfinales($convocatoria_id)
    {
        $finales = DB::table('requerimientos')->where('requerimientos.convocatoria_id', $convocatoria_id)
            ->join('postulants', 'postulants.requerimiento_id', 'requerimientos.id')
            ->join('nota_meritos', 'nota_meritos.postulante_id', 'postulants.id')
            ->join('convocatorias', 'convocatorias.id', 'requerimientos.convocatoria_id')
            ->select('postulants.name', 'postulants.last_name', 'postulants.cod_sis', 'requerimientos.destino', 'postulants.nota_conocimientos', 'nota_meritos.nota', 'postulants.nota_final', 'convocatorias.publicacion_finales')
            ->where('convocatorias.publicacion_finales', 2)
            ->where('postulants.id_status', 2)
            ->get();

            $inhabilitados = DB::table('requerimientos')->where('requerimientos.convocatoria_id', $convocatoria_id)
            ->join('postulants', 'postulants.requerimiento_id', 'requerimientos.id')
            ->join('nota_meritos', 'nota_meritos.postulante_id', 'postulants.id')
            ->join('convocatorias', 'convocatorias.id', 'requerimientos.convocatoria_id')
            ->select('postulants.name', 'postulants.last_name', 'postulants.cod_sis', 'requerimientos.destino', 'postulants.nota_conocimientos', 'nota_meritos.nota', 'postulants.nota_final', 'postulants.observaciones', 'convocatorias.publicacion_finales')
            ->where('postulants.id_status', 3)
            ->get();

        return view('vistaResulFinales', compact('finales' , 'inhabilitados'));
    }
}
