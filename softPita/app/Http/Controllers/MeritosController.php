<?php

namespace App\Http\Controllers;

use App\Meritos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MeritosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($convocatoria_id)
    {
        $meritos = DB::table('meritos')->where('convocatoria_id', $convocatoria_id)->get();

        return View("meritos/index", ['meritos' => $meritos, 'convocatoria_id' => $convocatoria_id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($convocatoria_id)
    {
        return View("meritos/create", ['convocatoria_id' => $convocatoria_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $convocatoria_id)
    {
        $campos = [
            'nombre' => 'required|string|max:255',
            'porcentaje' => 'required|integer|min:1|max:100',
        ];
        $Mensaje = ["required" => 'El :attribute es requerido'];

        $this->validate($request, $campos, $Mensaje);

        $nombre = $request->input("nombre");
        $porcentaje = $request->input("porcentaje");
        $merito_nuevo = new Meritos();
        $merito_nuevo->convocatoria_id = $convocatoria_id;
        $merito_nuevo->nombre = $nombre;
        $merito_nuevo->porcentaje = $porcentaje;
        $merito_nuevo->save();
        return redirect(route("detalle.index", ['convocatoria_id' => $convocatoria_id]))->with('Mensaje', 'Merito creado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function show(Meritos $merito)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function edit($convocatoria_id, $merito_id)
    {
        $merito = Meritos::findOrFail($merito_id);

        return View("meritos/edit", ['merito' => $merito, 'convocatoria_id' => $convocatoria_id, 'merito_id' => $merito_id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $convocatoria_id, $merito_id)
    {
        $campos = [
            'nombre' => 'required|string|max:255',
            'porcentaje' => 'required|integer|min:1|max:100',
        ];
        $Mensaje = ["required" => 'El :attribute es requerido'];

        $this->validate($request, $campos, $Mensaje);

        $nombre = $request->input("nombre");
        $porcentaje = $request->input("porcentaje");
        $merito_nuevo = Meritos::findOrFail($merito_id);
        $merito_nuevo->convocatoria_id = $convocatoria_id;
        $merito_nuevo->nombre = $nombre;
        $merito_nuevo->porcentaje = $porcentaje;
        $merito_nuevo->save();
        return redirect(route("detalle.index", ['convocatoria_id' => $convocatoria_id]))->with('Mensaje', 'Merito modificado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function destroy($convocatoria_id, $merito_id)
    {
        Meritos::destroy($merito_id);
        return redirect(route("detalle.index", ['convocatoria_id' => $convocatoria_id]))->with('Mensaje', 'Merito eliminado con exito');
    }
}
