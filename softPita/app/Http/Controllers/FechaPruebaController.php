<?php

namespace App\Http\Controllers;

use App\Convocatoria;
use App\FechaPrueba;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FechaPruebaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($convocatoria_id)
    {
        $fechas = DB::table('fecha_pruebas')->where('convocatoria_id', $convocatoria_id)->orderBy('fecha', 'ASC')->get();

        return View("fechas/index", ['fechas' => $fechas, 'convocatoria_id' => $convocatoria_id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($convocatoria_id)
    {
        return View("fechas/create", ['convocatoria_id' => $convocatoria_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $convocatoria_id)
    {
        $convocatoria = Convocatoria::find($convocatoria_id);

        $campos = [
            'fecha' => 'required|date|after:' . $convocatoria->fecha_publicacion,
            'evento' => 'required|string|max:55',
        ];
        $Mensaje = ["required" => 'El :attribute es requerido', "after" => 'La fecha debe ser despues de la fecha de publicacion ' . $convocatoria->fecha_publicacion];

        $this->validate($request, $campos, $Mensaje);

        $fecha = $request->input("fecha");
        $evento = $request->input("evento");
        $fecha_nuevo = new FechaPrueba();
        $fecha_nuevo->convocatoria_id = $convocatoria_id;
        $fecha_nuevo->fecha = $fecha;
        $fecha_nuevo->evento = $evento;
        $fecha_nuevo->save();
        return redirect(route("detalle.index", ['convocatoria_id' => $convocatoria_id]))->with('Mensaje', 'Fecha/evento creado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FechaPrueba  $fechaPrueba
     * @return \Illuminate\Http\Response
     */
    public function show(FechaPrueba $fechaPrueba)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FechaPrueba  $fechaPrueba
     * @return \Illuminate\Http\Response
     */
    public function edit($convocatoria_id, $fecha_id)
    {
        $fecha = FechaPrueba::findOrFail($fecha_id);

        return View("fechas/edit", ['fecha' => $fecha, 'convocatoria_id' => $convocatoria_id, 'fecha_id' => $fecha_id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FechaPrueba  $fechaPrueba
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $convocatoria_id, $fecha_id)
    {
        $convocatoria = Convocatoria::find($convocatoria_id);
        $campos = [
            'fecha' => 'required|date|after:' . $convocatoria->fecha_publicacion,
            'evento' => 'required|string|max:255',
        ];
        $Mensaje = ["required" => 'El :attribute es requerido', "after" => 'La fecha debe ser despues de la fecha de publicacion ' . $convocatoria->fecha_publicacion];

        $this->validate($request, $campos, $Mensaje);

        $fecha = $request->input("fecha");
        $evento = $request->input("evento");
        $fecha_nuevo = FechaPrueba::findOrFail($fecha_id);
        $fecha_nuevo->convocatoria_id = $convocatoria_id;
        $fecha_nuevo->fecha = $fecha;
        $fecha_nuevo->evento = $evento;
        $fecha_nuevo->save();
        return redirect(route("detalle.index", ['convocatoria_id' => $convocatoria_id]))->with('Mensaje', 'Fecha/evento modificado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FechaPrueba  $fechaPrueba
     * @return \Illuminate\Http\Response
     */
    public function destroy($convocatoria_id, $fecha_id)
    {
        FechaPrueba::destroy($fecha_id);
        return redirect(route("detalle.index", ['convocatoria_id' => $convocatoria_id]))->with('Mensaje', 'Fecha/evento eliminado con exito');
    }
}
