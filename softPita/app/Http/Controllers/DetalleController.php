<?php

namespace App\Http\Controllers;

use App\Detalle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DetalleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($convocatoria_id)
    {
        $titulos = DB::table('convocatorias')->find($convocatoria_id);
        $requerimientos = DB::table('requerimientos')->where('convocatoria_id', $convocatoria_id)->get();
        $requisitos = DB::table('requisitos')->where('convocatoria_id', $convocatoria_id)->get();
        $documentos = DB::table('documentos')->where('convocatoria_id', $convocatoria_id)->get();
        $fechas = DB::table('fecha_pruebas')->where('convocatoria_id', $convocatoria_id)->get();
        $meritos = DB::table('meritos')->where('convocatoria_id', $convocatoria_id)->get();
        $meritos1 = DB::table('meritos1s')->where('convocatoria_id', $convocatoria_id)->get();
        $mertos = DB::table('mertos')->where('convocatoria_id', $convocatoria_id)->get();
        $subunidades = DB::table('subunidades')->where('subunidades.id', $titulos->subunidad_id)->first();
        $departamentos = DB::table('departamentos')->where('departamentos.id', $subunidades->id_departamento)->first();
        return View("detalles.index", [ 'titulos' => $titulos, 'requerimientos' => $requerimientos, 'requisitos' => $requisitos, 'fechas' => $fechas, 'documentos' => $documentos, 'meritos' =>$meritos,'meritos1' =>$meritos1, 'mertos' =>$mertos, 'convocatoria_id' => $convocatoria_id, 'departamentos' => $departamentos ]);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Detalle  $detalle
     * @return \Illuminate\Http\Response
     */
    public function show(Detalle $detalle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Detalle  $detalle
     * @return \Illuminate\Http\Response
     */
    public function edit(Detalle $detalle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Detalle  $detalle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Detalle $detalle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Detalle  $detalle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Detalle $detalle)
    {
        //
    }
}
