<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Convocatoria extends Model
{
    protected $connection = 'mysql';
    public $timestamps = false;
    protected $fillable = [

        'titulo_convocatoria','fecha_publicacion', 'porcentaje_conocimiento', 'porcentaje_meritos', 'subunidad_id', 'comision_conocimiento', 'comision_merito'
    ];
    public static function convocatorias($id){
        return Convocatoria::where('subunidad_id', '=', $id)->where('convocatorias.estado', 'Habilitada')->orderBy('fecha_publicacion', 'DESC')->get();
    }

    public static function convocatoriasNew($id){
        return Convocatoria::where('subunidad_id', '=', $id)->where('convocatorias.estado', 'Inhabilitada')->orderBy('fecha_publicacion', 'DESC')->get();
    }

    public static function convocatoriasP($id){
        $datos =Convocatoria::where('subunidad_id', '=', $id)->where('convocatorias.estado', 'Habilitada')->orderBy('fecha_publicacion', 'DESC')->first();
        
        return Requerimiento::where('convocatoria_id', $datos->id)->get();
    }
    public function requerimientos(){

        return $this->hasMany(Requerimiento::class);
    }

    public function requesitos(){

        return $this->hasMany(Requisito::class);
    }
    public function documentos(){

        return $this->hasMany(Documento::class);
    }
    public function fechas(){

        return $this->hasMany(FechaPrueba::class);
    }
    public function notaConocimiento(){

        return $this->hasMany(NotaConocimiento::class);
    }
   
}
