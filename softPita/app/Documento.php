<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documento extends Model
{
    protected $connection = 'mysql';

    public $timestamps = false;

    protected $fillable = [

        'convocatoria_id', 'documento'
    ];

    public function convocatoria(){

        return $this->belongsTo(Convocatoria::class);
    }
}
