<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FechaPrueba extends Model
{
    protected $connection = 'mysql';

    public $timestamps = false;
    protected $fillable = [

        'convocatoria_id', 'fecha', 'evento'
    ];

    public function convocatoria(){

        return $this->belongsTo(Convocatoria::class);
    }
}
