@extends('layouts.app2')
<!-- link boostrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@section('content')

<!-- inicio notas merito -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><br>
                    <h2 class="text-center">NOTAS MERITO</h2><br><br>
                    <p> <strong> Nota: Mientras los resultados de meritos no se publiquen, no se mostraran en la seccion de avisos.</strong> </p>
                    <!-- boton publicar notas meritos -->
                    @if($convocatoria->publicacion_meritos == 1)
                    <p> <strong> Los resultados de meritos de esta convocatoria no se publicaron aun </strong> </p>
                    <a class="btn btn-success text-white" href="{{ url('notasMerito/meritos/'.$convocatoria->id)}}">Publicar Resultados</a>
                    <!-- boton dejar de publicar notas meritos -->
                    @elseif($convocatoria->publicacion_meritos == 2)
                    <p> <strong> Los resultados de meritos de esta convocatoria se estan publicando </strong> </p>
                    <a class="btn btn-danger text-white" href="{{ url('notasMerito/meritos/'.$convocatoria->id)}}">Dejar de publicar Resultados</a>
                    @endif
                    <!-- boton volver -->
                    <a class="btn btn-secondary float-right" href="{{ route('notasMerito.index')}}">Volver</a><br>

                    <div class="form-group"><br>
                        <!-- tabla de postulantes y notas -->
                        <table class="table table-hover table-bordered">
                            <thead class="thead-light">
                                <tr>
                                    <th>Apellidos</th>
                                    <th>Nombres</th>
                                    <th>Item</th>
                                    <th>Nota</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($postulantes as $postulante)
                                <tr>
                                    <td>{{$postulante->last_name}}</td>
                                    <td>{{$postulante->name}}</td>
                                    <td>{{$postulante->destino}}</td>
                                    <td>{{$postulante->nota}}</td>
                                    <td>
                                        <!-- boton calificar -->
                                        <a class="btn btn-info" href="{{ route('notasMerito.edit', ['convocatoria_id'=>$convocatoria_id, 'postulante_id' => $postulante->id ])}}">Calificar</a>
                                    </td>
                                </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection