<script>
    /* calculo total de notas */
    function calcular_total() {
        nota_final = 0
        nota_final1 = 0
        nota_final2 = 0
        nota_final3 = 0
        $(".nota_parcial").each(
            function(index, value) {
                nota_final = nota_final + eval($(this).val());
            }
        );
        $("#total").val(nota_final1);

        $(".nota_parcial1").each(
            function(index, value) {
                nota_final1 = nota_final1 + eval($(this).val());
            }
        );
        $("#total1").val(nota_final1);

        $(".nota_parcial2").each(
            function(index, value) {
                nota_final2 = nota_final2 + eval($(this).val());
            }
        );
        $("#total2").val(nota_final2);

        $(".nota_parcial3").each(
            function(index, value) {
                nota_final3 = nota_final3 + eval($(this).val());
            }
        );
        $("#total3").val(nota_final3);
    }
</script>

<!-- tabla de notas merito del postulante a calificar -->
<table class="table table-bordered table-hover">
    <div class="form-group"><br>
        <!-- se recuperan nombres y porcentaje de notas -->
        <h4>Nombre del postulante:</h4>
        {{ $notasMerito->name }}<br><br>
        <h4>Nota actual:</h4>
        <p>{{ $notasMerito->nota }}</p>
    </div>
    <!-- asignacion y verificacion de notas 1er grupo -->
    <thead class="thead-light">
        <tr>
            <th>Rendimiento Academico </th>
            @php
            $suma2=0;
            @endphp
            <th>
                @foreach($meritos as $merito)
                @php
                $suma2+=$merito->porcentaje;
                @endphp
                @endforeach
                {{$suma2}}
                pts.</th>
            <th>Nota</th>
        </tr>
    </thead>
    <tbody>
        @php
        $resultado=0;
        $resultadoFinal=0;
        $contador= 10;
        $cadena="";
        @endphp
        @foreach($meritos as $merito)
        <tr>
            @php
            $contador++;
            $resultado = $merito->porcentaje /100;
            $resultadoFinal += $resultado;
            $cadena='porcentaje' . $contador;
            @endphp
            <td>{{$merito->nombre}}</td>
            <td>
                <input type="hidden" name="porcentaje" value="{{$resultadoFinal}}" checked>{{$merito->porcentaje}} pts.</td>
            <td>
                <input type="number" class="nota_parcial1 form-control" name="nota" required min="1" max="{{$merito->porcentaje}}">
            </td>

        </tr>
        @endforeach
    </tbody>
    <!-- asignacion y verificacion de notas 2do grupo -->
    <thead class="thead-light">
        <tr>
            <th>Documentos de experiencia universitaria</th>
            @php
            $suma1=0;
            @endphp
            <th>
                @foreach($meritos1 as $merito1)
                @php
                $suma1+=$merito1->porcentaje;
                @endphp
                @endforeach
                {{$suma1}}
                pts.</th>
            <th>Nota</th>
        </tr>
    </thead>
    @php
    $contador1 = 0;
    $resultado1 = 0;
    $resultadoFinal1 = 0;
    @endphp
    <tbody>
        @foreach($meritos1 as $merito1)
        <tr>
            @php
            $contador1++;
            $resultado1 = $merito1->porcentaje /100;
            $resultadoFinal1 += $resultado1;
            @endphp
            <td>{{$merito1->nombre}}</td>
            <td>
                <input type="hidden" name="porcentaje1" value="{{$resultadoFinal1}}" checked>{{$merito1->porcentaje}} pts.</td>
            <td>
                <input type="number" class="nota_parcial2 form-control" name="nota" required min="1" max="{{$merito1->porcentaje}}">
            </td>

        </tr>
        @endforeach
    </tbody>
    <!-- asignacion y verificacion de notas 3er grupo -->
    <thead class="thead-light">
        <tr>
            <th>Documentos de experiencia extrauniversitaria</th>
            @php
            $suma=0;
            @endphp
            <th>
                @foreach($mertos as $merto)
                @php
                $suma+=$merto->porcentaje;
                @endphp
                @endforeach
                {{$suma}}
                pts.</th>
            <th>Nota</th>
        </tr>
    </thead>
    <tbody>
        @php
        $resultado2=0;
        $resultadoFinal2=0;
        $contador2 = 0;
        @endphp
        @foreach($mertos as $merto)
        <tr>
            @php
            $contador2++;
            $resultado2 = $merto->porcentaje /100;
            $resultadoFinal2 += $resultado2;
            @endphp
            <td>{{$merto->nombre}}</td>
            <td>
                <input type="hidden" name="porcentaje2" value="{{$resultadoFinal2}}" checked>{{$merto->porcentaje}} pts.</td>
            <td>
                <input type="number" class="nota_parcial3 form-control" name="nota" required min="1" max="{{$merto->porcentaje}}">
            </td>

        </tr>
        @endforeach
        <input type="hidden" id="total1" name="total1" min="1" max="100" class="form-control" value="0" />
        <input type="hidden" id="total2" name="total2" min="1" max="100" class="form-control" value="0" />
        <input type="hidden" id="total3" name="total3" min="1" max="100" class="form-control" value="0" />
    </tbody>
</table>

<!-- boton guardar -->
<div class="form-group">
    <form method=post onsubmit="return doSomething()">
        <input type=submit class="btn btn-success" value="Guardar" onclick="calcular_total()">
    </form>