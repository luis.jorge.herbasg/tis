@extends('layouts.app2')
@section('content')

<!-- link bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<!-- formulario para agregar nota de examenes que se calificaran -->
<div class="container h5"><br>
  <h2 class="text-center">AGREGAR EVALUACION</h2><br><br>
  <form method="POST" action="{{ route('nota.store', [ 'convocatoria_id' =>$convocatoria_id, 'requerimiento_id' =>$requerimiento_id ]) }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
      <!-- label: nombre evaluacion -->
      <label for="">Nombre evaluacion:</label>
      <!-- input: nombre evaluacion -->
      <input type="text" class="form-control {{$errors->has('nombre_nota')?'is-invalid':'' }}" name="nombre_nota" value="{{ isset($nota->nombre_nota) ? $nota->nombre_nota:old('nombre_nota')}}" required></input>
      {!! $errors->first('nombre_nota','<div class="invalid-feedback">:message</div>') !!}
    </div>

    <div class="form-group">
      <!-- label: porcentaje evaluacion -->
      <label for="">Porcentaje Evaluacion:</label>
      <!-- input: porcentaje evaluacion -->
      <input type="number" class="form-control {{$errors->has('porcentaje')?'is-invalid':'' }}" name="porcentaje" value="{{ isset($nota->porcentaje) ? $nota->porcentaje:old('porcentaje')}}" required max="100" min="1"></input>
      {!! $errors->first('porcentaje','<div class="invalid-feedback">:message</div>') !!}
    </div>

    <div>
      <!-- botones guardar y cancelar -->
      <button class="btn btn-success" type="submit">Guardar</button>
      <a href="{{ route('nota.index', [ 'convocatoria_id' =>$convocatoria_id, 'requerimiento_id' =>$requerimiento_id ]) }}" class="btn btn-secondary">Cancelar</a>
    </div>
  </form>
</div>

@endsection