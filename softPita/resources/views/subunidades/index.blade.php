<title>SubUnidades</title>
@extends('layouts.app2')
<!-- link bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
@section('content')
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<!-- inicio subunidades -->
<div class="container"><br>
    <table class="table table-bordered table-hover">
        <h2 class="text-center">SUB UNIDADES</h2><br>
        <!-- botones regresar a departamentos y agregar subunidades -->
        <a class="btn btn-secondary float-right" href="{{url('departamentos')}}">Regresar A Departamentos</a>
        <a class="btn float-right hide"></a>
        <a class="btn float-right" style="background:#172962; color:white" href="{{ route('subunidad.create', [ 'id_departamento' => $id_departamento ]) }}">Agregar Subunidades</a><br><br>
        <!-- tabla subunidades -->
        <table class="table table-hover table-bordered text-center">
        <thead class="thead-light">
            <tr>
                <th>#</th>
                <th>Nombre</th>
                <th></th>
                <th>Acciones</th>
        </thead>
        </tr>
        @foreach($unidades as $unidad)
        <tr>
            @foreach($cabeceras as $cabeza)
            <td>{{ $unidad->$cabeza }}</td>
            @endforeach
            <td>
                <form method="post" action="{{route('subunidad.destroy', [ 'id_dep'=> $id_departamento, 'id_sub' => $unidad->id ])}}">
                    <!-- botones editar, eliminar y agregar item -->
                    <a class="btn btn-warning text-white" href="{{route('subunidad.edit', [ 'id_dep'=> $id_departamento, 'id_sub' => $unidad->id ])}}">Editar</a>
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button type="submit" class="btn btn-danger" onclick="return confirm('¿Esta seguro de que desea eliminar el elemento?');" style="display:inline; background-color: #C92023">Eliminar</button>
                    <a href="{{route('items.index', ['subunidad_id' => $unidad->id, 'id_dep'=> $id_departamento])}}" class="btn" style="display:inline; background:#172962; color:white">Agregar Item</a>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
</div><br>

@endsection