@extends('layouts.app2')
<!-- link bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@section('content')
<!-- visualizacion de usuario -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><br><br>
                    <div class="panel-body"><br>
                        <h2 class="text-center">USUARIO</h2><br><br>
                        <!-- datos usuario -->
                        <p><strong>Nombre:</strong> {{ $user->name }} </p>
                        <p><strong>Email:</strong> {{ $user->email }} </p>
                        <p><strong>Cod Sis:</strong> {{ $user->codSis }} </p>
                    </div><br>
                    <!-- boton volver -->
                    <a href="{{ URL::previous() }}" class="btn btn-secondary">Volver</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection