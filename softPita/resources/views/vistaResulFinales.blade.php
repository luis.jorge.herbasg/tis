@extends('layouts.app3')
<!-- link bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@section('content')
<!-- vista resultados finales HOME -->
<div class="text-center">
    <label for="" class="h2 text-center">Resultados Finales</label> <br> <br> <br>
</div>
<!-- tabla resultadosfinales -->
<table class="table table-hover table-bordered">
    <thead class="thead-light">
        <tr>
            <th scope="col" class="text-center">CODIGO SIS</th>
            <th scope="col" class="text-center">APELLIDO</th>
            <th scope="col" class="text-center">NOMBRE</th>
            <th scope="col" class="text-center">ASIGNATURA</th>
            <th scope="col" class="text-center">NOTA CONOCIMIENTO</th>
            <th scope="col" class="text-center">NOTA MERITOS</th>
            <th scope="col" class="text-center">NOTA FINAL</th>
        </tr>
    </thead>
    <tbody class="text-center">
        @foreach($finales as $final)
        <tr>
            <td>{{$final->cod_sis}}</td>
            <td>{{$final->last_name}}</td>
            <td>{{$final->name}}</td>
            <td>{{$final->destino}}</td>
            <td>{{$final->nota_conocimientos}}</td>
            <td>{{$final->nota}}</td>
            <td>{{$final->nota_final}}</td>
        </tr>
        @endforeach
    </tbody>
</table><br><br>

<div class="text-center">
    <label for="" class="h2 text-center">Postulantes Inhabilitados</label> <br> <br> <br>
</div>

<table class="table table-hover table-bordered">
    <thead class="thead-light">
        <tr>
            <th scope="col" class="text-center">CODIGO SIS</th>
            <th scope="col" class="text-center">APELLIDO</th>
            <th scope="col" class="text-center">NOMBRE</th>
            <th scope="col" class="text-center">ASIGNATURA</th>
            <th scope="col" class="text-center">ESTADO</th>
            <th scope="col" class="text-center">OBSERVACION</th>
        </tr>
    </thead>
    <tbody class="text-center">
        @foreach($inhabilitados as $Inhabilitado)
        <tr>
            <td>{{$Inhabilitado->cod_sis}}</td>
            <td>{{$Inhabilitado->last_name}}</td>
            <td>{{$Inhabilitado->name}}</td>
            <td>{{$Inhabilitado->destino}}</td>
            <td>Inhabilitado</td>
            <td>{{$Inhabilitado->observaciones}}</td>
        </tr>
        @endforeach
    </tbody>
</table><br><br>

@endsection