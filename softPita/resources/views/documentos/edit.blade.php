@extends('layouts.app2')
@section('content')

<!-- link de bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<!-- formulario para editar un documento en convocatoria -->
<div class="container h5"><br>
  <h2 class="text-center">EDITAR DOCUMENTO</h2><br><br>

  <form method="post" action="{{ route('documento.update', [ 'convocatoria_id' =>$convocatoria_id, 'documento_id'=> $documento->id ]) }}">
    {{ method_field('PUT') }}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
      <!-- label: documento -->
      <label for="">Documento:</label>
      <!-- input: documento -->
      <input type="text" class="form-control {{$errors->has('documento')?'is-invalid':'' }}" name="documento" value="{{ isset($documento->documento) ? $documento->documento:old('documento')}}" rows="3"></textarea>
      {!! $errors->first('documento','<div class="invalid-feedback">:message</div>') !!}
    </div>

    <div>
      <!-- botones guardar y cancelar -->
      <button class="btn btn-success" type="submit">Guardar</button>
      <a href="{{ route('detalle.index', [ 'convocatoria_id' =>$convocatoria_id ]) }} " class="btn btn-secondary">Cancelar</a>
    </div>

  </form>
</div>

@endsection