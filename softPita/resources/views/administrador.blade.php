@extends('layouts.app2')
@section('content')
<!-- vista de bienvenida a usuario -->
<section id="opciones">
  <div class="col text-uppercase text-center pb-2">
    <h2> <strong>BIENVENIDO</strong> </h2>
  </div>
  <div class="col-12 mb-2">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title text-center"><strong>Opciones de administraci&oacute;n</strong></h4>
        <p class="card-text text-center">
          Dentro de esta sección podrá realizar todas las acciones que necesite y para
          las cuales tenga permiso. Para manejar las funciones del sistema utilice el menú
          de la parte izquierda.
        </p>
      </div>
    </div>
  </div>
  </div>
  </div>
</section>
@endsection

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>