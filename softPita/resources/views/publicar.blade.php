@extends('layouts.app2')
<!-- link bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@section('content')

<!-- vista convocatorias vigentes AVISOS -->
<div class="text-center">
    <label for="" class="h2 text-center">Convocatorias Vigentes</label> <br> <br> <br>
</div>
<!-- tabla de convocatorias -->
<table class="table table-hover table-bordered">
    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th scope="col" class="text-center">Departamento</th>
            <th scope="col" class="text-center">Subunidad</th>
            <th scope="col" class="text-center">Titulo de la convocatoria</th>
            <th scope="col" class="text-center">Fecha de publicacion</th>
            <th scope="col" class="text-center">Accion</th>
        </tr>
    </thead>
    <tbody class="text-center">
        @foreach($convocatorias as $convocatoria)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$convocatoria->dep}}</td>
            <td>{{$convocatoria->name}}</td>
            <td>{{$convocatoria->titulo_convocatoria}}</td>
            <td>{{$convocatoria->fecha_publicacion}}</td>
            <td>
                <a class="btn btn-info" href="{{ route('visualizar.indexAdmin', [ 'convocatoria_id' =>$convocatoria->id ]) }}">Ver</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<br><br><br><br><br>

@endsection