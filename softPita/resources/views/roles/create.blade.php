@extends('layouts.app2')
<!-- link bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@section('content')
<!-- vista que mandar a form -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default"><br>
                <h2 class="text-center">CREAR ROL</h2><br><br>

                <div class="panel-body h5">
                    <!-- formulario para crear rol -->
                    {{ Form::open(['route' => 'roles.store']) }}
                    @include('roles.partials.form')
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection