@extends('layouts.app2')
<!-- link de bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@section('content')

<!-- inicio de roles -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><br>
                    <h2 class="text-center">ROLES</h2><br><br>
                    <!-- boton crear rol -->
                    <a href="{{ route('roles.create') }}" class="btn float-right" style="background:#172962; color:white">
                        Nuevo rol
                    </a>
                </div><br>
                <!-- tabla de roles -->
                <div class="panel-body"><br>
                    <table class="table table-bordered table-hover">
                        <thead class="thead-light">
                            <tr>
                                <th width="10px">ID</th>
                                <th>Nombre</th>
                                <th colspan="3">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- botones ver, personalizar y eliminar -->
                            @foreach($roles as $role)
                            <tr>
                                <td>{{ $role->id }}</td>
                                <td>{{ $role->name }}</td>
                                <td width="10px">
                                    <a href="{{ route('roles.show', $role->id) }}" class="btn btn-info">Ver</a>
                                </td>
                                <td width="10px">
                                    <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-warning text-white">Personalizar</a>
                                </td>
                                <td width="10px">
                                    {!! Form::open(['route' => ['roles.destroy', $role->id],
                                    'method' => 'DELETE']) !!}
                                    <button class="btn btn-danger" style="background-color: #C92023" onclick="return confirm('¿Esta seguro de que desea eliminar el elemento?');">Eliminar</button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $roles->render() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection