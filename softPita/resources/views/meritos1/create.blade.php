@extends('layouts.app2')
@section('content')

<!-- link bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<!-- formulario crear segundo grupo de meritos -->
<div class="container h5"><br>
    <h2 class="text-center">CREAR MERITO</h2><br><br>
    <form method="POST" action="{{ route('merito1.store', [ 'convocatoria_id' =>$convocatoria_id ]) }}">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <!-- label: nombre -->
            <label for="">Nombre:</label>
            <!-- input: nombre -->
            <input type="text" class="form-control {{$errors->has('nombre')?'is-invalid':'' }}" name="nombre" value="{{ isset($meritos->nombre) ? $meritos->nombre:old('nombre')}}">
            {!! $errors->first('nombre','<div class="invalid-feedback">:message</div>') !!}
        </div>

        <div class="form-group">
            <!-- label: porcentaje -->
            <label for="">Porcentaje:</label>
            <!-- input: porcentaje -->
            <input type="text" class="form-control {{$errors->has('porcentaje')?'is-invalid':'' }}" name="porcentaje" value="{{ isset($meritos->porcentaje) ? $meritos->porcentaje:old('porcentaje')}}">
            {!! $errors->first('porcentaje','<div class="invalid-feedback">:message</div>') !!}
        </div>

        <div>
            <!-- botones guardar y cancelar -->
            <button class="btn btn-success" type="submit">Guardar</button>
            <a href="{{ route('detalle.index', [ 'convocatoria_id' =>$convocatoria_id ]) }} " class="btn btn-secondary">Cancelar</a>
        </div>
    </form>
</div>

@endsection