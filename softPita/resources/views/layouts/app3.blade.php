<!DOCTYPE html>
<html lang="es">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

  <title>Administrador de Convocatorias</title>
  <!-- estilos -->
  <style>
    #header {
      background-color: #172962 !important;
    }

    #footer {
      background-color: #172962 !important;
    }

    #footerCopyright {
      background-color: #020B28 !important;
    }

    .site-footer {
      background-color: #172962;
      padding: 45px 0 20px;
      font-size: 15px;
      line-height: 24px;
      color: #737373;
    }

    .site-footer h6 {
      color: #fff;
      font-size: 16px;
      text-transform: uppercase;
      margin-top: 5px;
      letter-spacing: 2px
    }

    .site-footer a {
      color: #737373;
    }

    .footer-links {
      padding-left: 0;
      list-style: none
    }

    .footer-links a {
      color: #737373
    }

    #header img {
      width: 40px;
    }

    #main .carousel-inner img {
      max-height: 70vh;
      object-fit: cover;
    }

    #carousel {
      position: relative;
    }

    #carousel .overlay {
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      background-color: rgba(0, 0, 0, 0.5);
      color: white;
      z-index: 1;
    }

    #carousel .overlay .container,
    #carousel .overlay .row {
      height: 100%;
    }

    #footer a {
      color: white;
    }

    #footerCopyright p {
      color: white;
    }

    #headerCard {
      color: #EF0B0B;

    }

    h5 {
      color: #284DC4;
    }

    #opcionesTitle {
      color: #172962;
    }
  </style>
</head>

<body data-spy="scroll" data-target="#navbar" data-offset="66">
  <!-- navbar responsivo -->
  <nav id="header" class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
      <a class="navbar-brand" href="/">Administrador de Convocatorias</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbar">
        <ul class="navbar-nav ml-auto">
          @if (Auth::guest())
          <li class="nav-item">
            <a class="nav-link text-white" href="{{ route('login') }}">Iniciar sesión</a>
          </li>
          @else
          <li class="dropdown nav-item">
            <a href="#" class="dropdown-toggle nav-link text-white" data-toggle="dropdown" role="button" aria-expanded="false">
              {{ Auth::user()->name }} <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                  Salir
                </a>
                <!-- lista desplegable de sesion -->
                <div class="dropdown-divider"></div>
                <a href="/administrador">Administracion</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form>
              </li>
            </ul>
          </li>
          @endif
        </ul>
      </div>

    </div>
  </nav><br>
  
  <!-- cards -->
  <main id="main">
    <div class="container-fluid d-flex">
      <div class="container" style="max-width:1170px;">
        <div class="card-deck d-flex justify-content-center">
          <div class="card">
            <div class="card-block">
              <div class="container">

                @if(session('info'))
                <div class="container">
                  <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                      <div class="alert alert-success">
                        {{ session('info') }}
                      </div>
                    </div>
                  </div>
                </div>
                @endif

                @yield('content')
                <br>
                <br>
                <br>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div><br>
  </main>

  <!-- Footer -->
  <footer class="site-footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-6">
          <h6>Sobre Nosotros</h6>
          <p class="text-justify">La Universidad Mayor de San Simón (UMSS) cubre el área de la educación superior con sus funciones de enseñanza-aprendizaje, investigación científica y tecnológica e interacción social universitaria.</p>
        </div>
        <!-- links a paginas amigas -->
        <div class="col-xs-12 col-md-6 text-md-right">
          <h6>Enlaces</h6>
          <ul class="footer-links">
            <li><a href="/sobreNosotros">Sobre Nosotros</a></li>
            <li><a target="_blank" href="http://www.cs.umss.edu.bo/">CS</a></li>
            <li><a target="_blank" href="http://www.fcyt.umss.edu.bo/">FCyT</a></li>
            <li><a target="_blank" href="http://sagaa.fcyt.umss.edu.bo/admision/noticias.php">SAGAA</a></li>
            <li><a target="_blank" href="http://websis.umss.edu.bo/home.asp">webSISS</a></li>
          </ul>
        </div>
      </div>
      <hr>
    </div>
    <div class="container">
      <div class="row">
        <div class="col d-flex justify-content-center text-white">
          <p class="copyright-text">Derechos Reservados © 2020 · Universidad Mayor de San Simón · SoftpitaDmani
          </p>
        </div>
      </div>
    </div>
  </footer>

</html>
<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
@yield('script1')
@yield('script')

</body>

</html>