<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostulants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('postulants', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('requerimiento_id');
            $table->foreign('requerimiento_id')->references('id')->on('requerimientos')->onDelete('cascade');
            $table->integer('cod_sis');
            $table->string('name', 100);
            $table->string('last_name', 100);
            $table->string('address', 100);
            $table->integer('phone');
            $table->string('email',100);
            $table->integer('nro_documents');
            $table->integer('nro_certificates');
            $table->integer('id_status')->unsigned()->default(1);
            $table->text('observaciones');
            $table->integer('nota_conocimientos')->default(0);
            $table->integer('nota_final')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postulants');
    }
}
