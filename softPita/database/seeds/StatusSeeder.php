<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Estados posibles de un postulante
        DB::table('postulant_status')->insert([
            'status' => 'EN ESPERA'
        ]
        );

        DB::table('postulant_status')->insert([
            'status' => 'HABILITADO'
        ]
        );

        DB::table('postulant_status')->insert([
            'status' => 'INHABILITADO'
        ]
        );
    }
}
