<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Usuarios
        Permission::create([
            'name'          => 'Navegar usuarios',
            'slug'          => 'users.index',
            'description'   => 'Lista y navega todos los usuarios del sistema',
        ]);
        Permission::create([
            'name'          => 'Ver detalle de usuario',
            'slug'          => 'users.show',
            'description'   => 'Ver en detalle cada usuario del sistema',
        ]);
        Permission::create([
            'name'          => 'Creación de usuario',
            'slug'          => 'users.create',
            'description'   => 'Crear un usuario para el sistema',
        ]);
        Permission::create([
            'name'          => 'Edición de usuario',
            'slug'          => 'users.edit',
            'description'   => 'Editar cualquier dato de un usuario del sistema',
        ]);
        Permission::create([
            'name'          => 'Eliminar usuario',
            'slug'          => 'users.destroy',
            'description'   => 'Eliminar cualquier usuario del sistema',
        ]);

        //Roles
        Permission::create([
            'name'          => 'Navegar roles',
            'slug'          => 'roles.index',
            'description'   => 'Lista y navega todos los roles del sistema',
        ]);
        Permission::create([
            'name'          => 'Ver detalle de rol',
            'slug'          => 'roles.show',
            'description'   => 'Ver en detalle cada rol del sistema',
        ]);
        Permission::create([
            'name'          => 'Creación de rol',
            'slug'          => 'roles.create',
            'description'   => 'Crear un rol del sistema',
        ]);
        Permission::create([
            'name'          => 'Edición de rol',
            'slug'          => 'roles.edit',
            'description'   => 'Editar cualquier dato de un rol del sistema',
        ]);
        Permission::create([
            'name'          => 'Eliminar rol',
            'slug'          => 'roles.destroy',
            'description'   => 'Eliminar cualquier rol del sistema',
        ]);

        //Convocatorias
        Permission::create([
            'name'          => 'Navegar convocatorias',
            'slug'          => 'convocatorias.index',
            'description'   => 'Lista y navega todas las convocatorias del sistema',
        ]);
        Permission::create([
            'name'          => 'Ver convocatoria',
            'slug'          => 'visualizar.indexAdmin',
            'description'   => 'Ver una convocatoria de la lista',
        ]);
        Permission::create([
            'name'          => 'Creación de convocatoria',
            'slug'          => 'convocatorias.create',
            'description'   => 'Editar cualquier dato de una convocatoria del sistema',
        ]);
        Permission::create([
            'name'          => 'Edición de convocatoria',
            'slug'          => 'convocatorias.edit',
            'description'   => 'Editar cualquier dato de una convocatoria del sistema',
        ]);
        Permission::create([
            'name'          => 'Eliminar convocatoria',
            'slug'          => 'convocatorias.destroy',
            'description'   => 'Eliminar cualquier convocatoria del sistema',
        ]);
        Permission::create([
            'name'          => 'Agregar detalles de la convocatoria',
            'slug'          => 'detalle.index',
            'description'   => 'Agregar requerimientos, requisitos, documentos y fechas a la convocatoria',
        ]);

        //Permisos Postulantes 
         Permission::create([
            'name'          => 'Navegar postulantes',
            'slug'          => 'listaspostulantes.index',
            'description'   => 'Lista y navega todos los postulantes del sistema',
        ]);

        Permission::create([
            'name'          => 'Habilitar Postulante',
            'slug'          => 'documentos.show',
            'description'   => 'Habilita o inhabilita un postulante',
        ]);

        //Registro de notas conocimiento
        Permission::create([
            'name'          => 'Navegar notas de conocimiento',
            'slug'          => 'conocimiento.index',
            'description'   => 'Lista y navega todas las notas de conocimiento del sistema del respectivo dueño e ítem',
        ]);
    

        Permission::create([
            'name'          => 'Edición de notas de conocimiento',
            'slug'          => 'conocimiento.ingresar',
            'description'   => 'Ingresar una nota de conocimiento',
        ]);

        Permission::create([
            'name'          => 'Edición de nombres de examen de conocimiento',
            'slug'          => 'nota.index',
            'description'   => 'Permite editar, agregar, eliminar examenes de notas de conocimiento',
        ]);

        //Registro de notas merito
        Permission::create([
            'name'          => 'Navegar notas de merito',
            'slug'          => 'merito.index',
            'description'   => 'Lista y navega todas las notas de merito del sistema del respectivo dueño e ítem',
        ]);
        Permission::create([
            'name'          => 'Edición de notas de merito',
            'slug'          => 'merito.edit',
            'description'   => 'Editar cualquier nota de merito',
        ]);

        //Notas finales
        Permission::create([
            'name'          => 'Navegar notas finales',
            'slug'          => 'finales.index',
            'description'   => 'Lista y navega todas las notas finales del sistema',
        ]);

        //Departamentos 
        Permission::create([
            'name'          => 'Ver departamentos del sistema',
            'slug'          => 'departamentos.show',
            'description'   => 'Navegar y visualizar todos los departamentos del sistema',
        ]);
        Permission::create([
            'name'          => 'Crear un departamento',
            'slug'          => 'departamentos.create',
            'description'   => 'Crear un nuevo departamento',
        ]);
        Permission::create([
            'name'          => 'Editar un departamento',
            'slug'          => 'departamentos.edit',
            'description'   => 'Editar cualquier departamento del sistema',
        ]);
        Permission::create([
            'name'          => 'Eliminar un departamento',
            'slug'          => 'departamentos.destroy',
            'description'   => 'Eliminar cualquier departamento del sistema',
        ]);
        

        //Subunidades subunidades.destroy
        Permission::create([
            'name'          => 'Ver subunidades del departamento',
            'slug'          => 'subunidades.index',
            'description'   => 'Visualizar todas las subunidades del departamento y acciones respectivas',
        ]);
        Permission::create([
            'name'          => 'Editar subunidad',
            'slug'          => 'subunidades.edit',
            'description'   => 'Editar cualquiera de las subunidades del departamento',
        ]);
        Permission::create([
            'name'          => 'Crear subunidad',
            'slug'          => 'subunidades.create',
            'description'   => 'Crear una nueva subunidad del departamento',
        ]);
        Permission::create([
            'name'          => 'Eliminar subunidad',
            'slug'          => 'subunidades.destroy',
            'description'   => 'Eliminar una subunidad del departamento',
        ]);

        //Items
        Permission::create([
            'name'          => 'Ver Items de una subunidad',
            'slug'          => 'items.index',
            'description'   => 'Ver items disponibles de una determinada subunidad',
        ]);
        Permission::create([
            'name'          => 'Crear Items',
            'slug'          => 'items.create',
            'description'   => 'Crear items para un departamento',
        ]);
        Permission::create([
            'name'          => 'Editar Items',
            'slug'          => 'items.edit',
            'description'   => 'Editar items del sistema',
        ]);
        Permission::create([
            'name'          => 'Eliminar Items',
            'slug'          => 'items.destroy',
            'description'   => 'Eliminar items del sistema',
        ]);
    }
}
