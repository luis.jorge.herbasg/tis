<?php

use Illuminate\Database\Seeder;
use App\Nota;
use App\TipoNota;
use App\Conocimiento;



class NotasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revisión de claves foráneas

        //Creamos los tipos de notas que tendrá la aplicación
        TipoNota::create(['nombre'    => 'CONOCIMIENTO']);
        TipoNota::create(['nombre'    => 'MERITO']);
        TipoNota::create(['nombre'    => 'FINAL']);

        //Creamos examenes ejemplo
        Nota::create([
            'requerimiento_id' => 1,
            'nombre_nota' => 'Primer Examen',
        ]);
        Nota::create([
            'requerimiento_id' => 1,
            'nombre_nota' => 'Examen de Linux',
        ]);
        Nota::create([
            'requerimiento_id' => 1,
            'nombre_nota' => 'Tercer examen',
        ]);
        Nota::create([
            'requerimiento_id' => 2,
            'nombre_nota' => 'Examen de conocimientos de Elementos de programación',
        ]);
        Nota::create([
            'requerimiento_id' => 2,
            'nombre_nota' => 'Examen de árboles binarios',
        ]);
        Nota::create([
            'requerimiento_id' => 3,
            'nombre_nota' => 'Examen de conocimientos de Investigación Operativa',
        ]);
        Nota::create([
            'requerimiento_id' => 4,
            'nombre_nota' => 'Examen de conocimientos de Investigación Operativa 2',
        ]);
        Nota::create([
            'requerimiento_id' => 5,
            'nombre_nota' => 'Auxiliar de laboratorio de computación',
        ]);
        Nota::create([
            'requerimiento_id' => 6,
            'nombre_nota' => 'Auxiliar de laboratorio mantenimiento',
        ]);

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Reactivamos la revisión de claves foráneas

    }
}
