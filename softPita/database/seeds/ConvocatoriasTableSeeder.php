<?php

use Illuminate\Database\Seeder;
use App\Convocatoria;
use App\Documento;
use App\FechaPrueba;
use App\Meritos;
use App\Meritos1;
use App\Mertos;
use App\Requerimiento;
use App\Requisito;


class ConvocatoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revisión de claves foráneas
        //Convocatorias
        Convocatoria::create([
            'subunidad_id' => 2,
            'titulo_convocatoria' => 'Convocatoria para auxiliares Inf. y Sistemas 2020 para materias pizarra',
            'fecha_publicacion' => '2020-07-21',
            'estado' => 'Habilitada',
            'publicacion_conocimientos' => 1,
            'publicacion_meritos' => 1
        ]);

        Convocatoria::create([
            'subunidad_id' => 3,
            'titulo_convocatoria' => 'Convocatoria para auxiliares Industrial 2020',
            'fecha_publicacion' => '2020-07-19',
            'estado' => 'Habilitada',
            'publicacion_conocimientos' => 1,
            'publicacion_meritos' => 1
        ]);

        Convocatoria::create([
            'subunidad_id' => 1,
            'titulo_convocatoria' => 'Convocatoria para auxiliares Inf. y Sistemas 2020 para laboratorios',
            'fecha_publicacion' => '2020-07-03',
            'estado' => 'Habilitada',
            'publicacion_conocimientos' => 1,
            'publicacion_meritos' => 1
        ]);

        //Documentos
        Documento::create([
            'convocatoria_id' => 1,
            'documento' => 'Fotocopia del CI'
        ]);

        Documento::create([
            'convocatoria_id' => 1,
            'documento' => 'Kardex'
        ]);

        Documento::create([
            'convocatoria_id' => 2,
            'documento' => 'Fotocopia del CI'
        ]);

        Documento::create([
            'convocatoria_id' => 3,
            'documento' => 'Fotocopia del CI'
        ]);

        Documento::create([
            'convocatoria_id' => 3,
            'documento' => 'Kardex'
        ]);

        //Fechas
        FechaPrueba::create([
            'convocatoria_id' => 1,
            'fecha' => '20/08/20',
            'evento' => 'Entrega de documentos'
        ]);

        FechaPrueba::create([
            'convocatoria_id' => 2,
            'fecha' => '21/08/20',
            'evento' => 'Entrega de documentos según formato'
        ]);

        FechaPrueba::create([
            'convocatoria_id' => 3,
            'fecha' => '22/08/20',
            'evento' => 'Entrega de documentos'
        ]);

        //Meritos 1
        Meritos::create([
            'convocatoria_id' => 1,
            'nombre' => 'Promedio de aprobacion de la materia a la que se postula',
            'porcentaje' => 35,
        ]);

        Meritos::create([
            'convocatoria_id' => 1,
            'nombre' => 'Promedio general de la materia',
            'porcentaje' => 30,
        ]);

        Meritos::create([
            'convocatoria_id' => 2,
            'nombre' => 'Promedio de aprobacion de la materia a la que postula',
            'porcentaje' => 30,
        ]);

        Meritos::create([
            'convocatoria_id' => 3,
            'nombre' => 'Conocimiento en Linux',
            'porcentaje' => 30,
        ]);

        Meritos::create([
            'convocatoria_id' => 3,
            'nombre' => 'Conocimiento en reparación de computadoras',
            'porcentaje' => 20,
        ]);

        //Meritos 2
        Meritos1::create([
            'convocatoria_id' => 1,
            'nombre' => 'Auxiliar de docencia del eje troncal',
            'porcentaje' => 15,
        ]);

        Meritos1::create([
            'convocatoria_id' => 1,
            'nombre' => 'Auxiliar en otras ramas o carreras',
            'porcentaje' => 5,
        ]);

        Meritos1::create([
            'convocatoria_id' => 1,
            'nombre' => 'Disertación cursillos y/o participación de proyectos',
            'porcentaje' => 5,
        ]);

        Meritos1::create([
            'convocatoria_id' => 2,
            'nombre' => 'Auxiliar de docencia del eje troncal',
            'porcentaje' => 35,
        ]);

        Meritos1::create([
            'convocatoria_id' => 2,
            'nombre' => 'Disertación cursillos y/o participación de proyectos',
            'porcentaje' => 10,
        ]);

        Meritos1::create([
            'convocatoria_id' => 3,
            'nombre' => 'Auxiliar en otras ramas o carreras',
            'porcentaje' => 20,
        ]);

        //Meritos 3
        Mertos::create([
            'convocatoria_id' => 1,
            'nombre' => 'Experiencia como operador, programador, analista',
            'porcentaje' => 5,
        ]);

        Mertos::create([
            'convocatoria_id' => 1,
            'nombre' => 'Experiencia docente en colegios, institutos, etc',
            'porcentaje' => 5,
        ]);

        Mertos::create([
            'convocatoria_id' => 2,
            'nombre' => 'Experiencia como operador',
            'porcentaje' => 5,
        ]);

        Mertos::create([
            'convocatoria_id' => 2,
            'nombre' => 'Experiencia docente en colegios, institutos, etc',
            'porcentaje' => 20,
        ]);

        Mertos::create([
            'convocatoria_id' => 3,
            'nombre' => 'Experiencia como operador, programador, analista',
            'porcentaje' => 15,
        ]);

        Mertos::create([
            'convocatoria_id' => 3,
            'nombre' => 'Experiencia docente en colegios',
            'porcentaje' => 15,
        ]);

        //Requerimientos
        Requerimiento::create([
            'convocatoria_id' => 1,
            'cantidad_auxiliares' => 4,
            'horas_academicas' => 6,
            'destino' => 'Introducción a la programación'
        ]);

        Requerimiento::create([
            'convocatoria_id' => 1,
            'cantidad_auxiliares' => 3,
            'horas_academicas' => 4,
            'destino' => 'Elementos de programación'
        ]);

        Requerimiento::create([
            'convocatoria_id' => 2,
            'cantidad_auxiliares' => 3,
            'horas_academicas' => 4,
            'destino' => 'Investigación Operativa'
        ]);

        Requerimiento::create([
            'convocatoria_id' => 2,
            'cantidad_auxiliares' => 6,
            'horas_academicas' => 4,
            'destino' => 'Investigación Operativa 2'
        ]);

        Requerimiento::create([
            'convocatoria_id' => 3,
            'cantidad_auxiliares' => 4,
            'horas_academicas' => 4,
            'destino' => 'Auxiliar de laboratorio de computación'
        ]);

        Requerimiento::create([
            'convocatoria_id' => 3,
            'cantidad_auxiliares' => 6,
            'horas_academicas' => 4,
            'destino' => 'Auxiliar de laboratorio mantenimiento'
        ]);

        //Requisitos
        Requisito::create([
            'convocatoria_id' => 1,
            'requisito' => 'Ser estudiante de la carrera'
        ]);

        Requisito::create([
            'convocatoria_id' => 2,
            'requisito' => 'Tener aprobadas todas las materias del nivel de la materia a la que desea postular'
        ]);

        Requisito::create([
            'convocatoria_id' => 2,
            'requisito' => 'Ser estudiante de la carrera'
        ]);

        Requisito::create([
            'convocatoria_id' => 3,
            'requisito' => 'Ser estudiante de la carrera'
        ]);

        Requisito::create([
            'convocatoria_id' => 3,
            'requisito' => 'Conocimiento básico en Linux'
        ]);

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Reactivamos la revisión de claves foráneas

    }
}
